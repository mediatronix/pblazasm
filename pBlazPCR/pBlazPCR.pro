QT += core
QT -= gui

CONFIG += c++11

TARGET = pBlazPCR
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    pBlazPCR.cpp \
    pbPicoCore.cpp

HEADERS += \
    pbPicoCore.h


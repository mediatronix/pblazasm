
QT += core
QT -= gui

QMAKE_CXX = gcc
QMAKE_CXXFLAGS = -x c

TARGET = pBlazASM
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11
TEMPLATE = app

DEFINES += GIT_VERSION=0x$$system(git describe --always)

INCLUDEPATH += ../common

SOURCES += \
    ../common/pbDirectives.h \
    ../common/dbuf.c \
    ../common/dbuf_string.c \
    ../common/getopt.c \
    ../common/pbLibGen.c \
    ../common/pbLib.c


HEADERS += \
    pbLexer.h \
    pbOpcodes.h \
    pbParser.h \
    pbSymbols.h \

SOURCES += \
    pBlazASM.c \
    pbLexer.c \
    pbParser.c \
    pbSymbols.c \

DISTFILES += \
    gpl-3.0.txt \
    sources.txt \
    ../../USBM13/PicoChaze/config.psm \
    ../../USBM13/PicoChaze/UART.psm \
    ../../USBM13/PicoChaze/Inband.psm \
    ../../USBM13/PicoChaze/USBMROM.psm \
    ../../USBM13/PicoChaze/FE100.psm \
    ../../USBM13/PicoChaze/USBMROM.lst \
    ../../../AppData/Roaming/QtProject/qtcreator/generic-highlighter/picoblaze.xml

